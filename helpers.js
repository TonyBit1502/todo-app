const moment = require('moment');
const PORT = process.env.PORT || 4000;

module.exports = {
    getStyleLink: function() {
        return `http://localhost:${PORT}/styles/index.css`
    },
    getScriptLink: function() {
        return `http://localhost:${PORT}/js/main.js`;
    },
    changeDateFormat: function(date) {
        return moment(date).format('MMMM DD, YYYY HH:mm');
    }
};
