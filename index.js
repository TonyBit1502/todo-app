const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');

const helpers = require('./helpers');
const config = require('./db.config');
const appRoutes = require('./routes/index');

const PORT = process.env.PORT || 4000;
const app = express();
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
    layoutsDir: path.join(__dirname, 'views/layouts/'),
    partialsDir: [
        path.join(__dirname, 'views/partials/'),
    ],
    helpers: helpers
});

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static('uploads'));

async function start() {
    try {
        await mongoose.connect(config, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });

        for (let routes of appRoutes) {
            app.use(routes);
        }

        app.listen(PORT, () => {
            console.log('Server has been started....');
        });
    } catch(err) {
        console.error(err);
    }
}

start();
