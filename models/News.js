const { Schema, model } = require('mongoose');
const schema = new Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    created_at: {
        type: Date
    },
    author: {
        type: String,
        required: true
    },
    is_published: {
        type: Boolean,
        default: false
    },
    newsImages: {
        type: [String],
        default: null
    },
    updated_at: {
      type: Date,
      default: null
    },
    published_at: {
        type: Date,
        default: null
    },
    deleted_at: {
        type: Date,
        default: null
    }
});

module.exports = model('News', schema);
