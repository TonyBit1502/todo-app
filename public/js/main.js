function setImagePreview(input) {
    if (input.files) {
        const reader = new FileReader();
        const parent = input.parentNode;
        const label = parent.getElementsByTagName('LABEL')[0];

        reader.onload = function(event) {
            const wrapper = document.createElement('DIV');
            const preview = document.createElement('IMG');
            const removeBtn = document.createElement('SPAN');

            preview.setAttribute('src', event.target.result);
            removeBtn.setAttribute('preview-idx', input.id);

            wrapper.classList.add('preview');
            removeBtn.classList.add('remove-image');

            removeBtn.addEventListener('click', removeImage);

            label.classList.add('hide');

            wrapper.appendChild(preview);
            wrapper.appendChild(removeBtn);
            parent.appendChild(wrapper);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function removeImage(event) {
    const parent = event.target.parentNode.parentNode;
    const rejectEl = parent.getElementsByTagName('DIV')[0];
    const label = parent.getElementsByTagName('LABEL')[0];
    const input = parent.getElementsByTagName('INPUT')[0];

    input.value = '';
    parent.removeChild(rejectEl);
    label.classList.remove('hide');
}

async function changeStatus(newsId, input) {
    try {
        const updateNewsPromise = await fetch(`http://localhost:4000/news/${newsId}/status`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({is_published: input.checked})
        });

        const status = await updateNewsPromise.json();

        if (status.success) {
            window.location.reload();
        }
    } catch (err) {
        console.error(err);
    }
}

async function removeNews(newsId) {
    try {
        const removeNewsPromise = await fetch(`http://localhost:4000/news/${newsId}`, {
            method: 'DELETE',
            body: ''
        });

        const status = await removeNewsPromise.json();

        if (status.success) {
            const parentEl = document.getElementsByClassName('news-list')[0];

            parentEl.removeChild(document.getElementById(newsId));
        }
    } catch (err) {
        console.error(err);
    }
}
