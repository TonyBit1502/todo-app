const mainRoutes = require('./main-route');
const todoRoutes = require('./todo-route');
const newsRoutes = require('./news-route');
const weatherRoutes = require('./weather-route');

module.exports = [mainRoutes, todoRoutes, newsRoutes, weatherRoutes];
