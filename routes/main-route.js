const { Router } = require('express');
const router = Router();

const Todo = require('../models/Todo');
const News = require('../models/News');

/**
 * Main page
 */
router.get('/', async (req, res) => {
    const todos = await Todo.find({});

    res.render('index', {
        title: 'Node JS App'
    });
});

module.exports = router;
