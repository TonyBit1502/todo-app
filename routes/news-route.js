const { Router } = require('express');
const multer = require('multer');
const News = require('../models/News');
const router = Router();

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().getTime() + file.originalname);
    }
});

const imageFilter = function (req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 50
    },
    fileFilter: imageFilter
});

router.get('/news', async (req, res) => {
    try {
        const news = await News.find();
        res.render('news', {
            title: 'News',
            news
        });
    } catch (err) {
        res.status(500).render('news', {
            title: 'News',
            message: err.message
        })
    }
});

router.get('/news/edit/:id', async (req, res) => {
    try {
        const news = await News.findById(req.params.id);

        res.render('news_item', {
            title: 'Edit news',
            news
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: err.message
        });
    }
});

router.get('/news/create', (req, res) => {
    res.render('news_item', {
        title: 'Create news',
        news: null
    });
});

router.post('/news/create', upload.fields([
    { name: 'img_1', maxCount: 1 },
    { name: 'img_2', maxCount: 1 },
    { name: 'img_3', maxCount: 1 },
    { name: 'img_4', maxCount: 1 },
    { name: 'img_5', maxCount: 1 },
    { name: 'img_6', maxCount: 1 },
    { name: 'img_7', maxCount: 1 },
    { name: 'img_8', maxCount: 1 }
]), async (req, res) => {
    const filePaths = [];

    for (let prop in req.files) {
        filePaths.push(req.files[prop][0].path);
    }

    try {
        const news = new News({
            title: req.body.title,
            content: req.body.description,
            created_at: new Date().toISOString(),
            author: req.body.author,
            newsImages: filePaths
        });

        await news.save();

        res.redirect('/news');
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: err.message
        });
    }
});

router.put('/news/:id', upload.fields([
    { name: 'img_1', maxCount: 1 },
    { name: 'img_2', maxCount: 1 },
    { name: 'img_3', maxCount: 1 },
    { name: 'img_4', maxCount: 1 },
    { name: 'img_5', maxCount: 1 },
    { name: 'img_6', maxCount: 1 },
    { name: 'img_7', maxCount: 1 },
    { name: 'img_8', maxCount: 1 }
]), async (res, req) => {
    if (!req.body) {
        res.status(400).json({
            message: 'News content can not be empty'
        });
    }

    try {
        const filePaths = [];

        for (let prop in req.files) {
            filePaths.push(req.files[prop][0].path);
        }

        const updateNews = await News.findByIdAndUpdate(req.params._id, {
            title: req.body.title,
            content: req.body.description,
            author: req.body.author,
            newsImages: filePaths,
            published_at: new Date().toISOString()
        }, {new: true});

        if (!updateNews) {
            return res.status(404).json({
                message: `News not found with id ${req.params.id}`
            });
        }

        res.json(updateNews);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: err.message
        });
    }
});

router.put('/news/:id/status', async (req, res) => {
    if (!req.body) {
        res.status(400).json({
            message: 'News content can not be empty'
        });
    }

    try {
        const updateNews = await News.findByIdAndUpdate(req.params.id, {
            is_published: !!req.body.is_published,
            published_at: new Date().toISOString()
        }, {new: true});

        if (!updateNews) {
            return res.status(404).json({
                message: `News not found with id ${req.params.id}`
            });
        }

        res.json({success: true});
    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: err.message
        });
    }
});

router.delete('/news/:id', async (req, res) => {
    try {
        await News.findByIdAndDelete(req.params.id);

        res.json({success: true});
    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: err.message
        });
    }

});

module.exports = router;
