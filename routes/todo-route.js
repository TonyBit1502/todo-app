const { Router } = require('express');
const router = Router();
const Todo = require('../models/Todo');

router.get('/todo_list', async (req, res) => {
    const todos = await Todo.find({});

    res.render('todo_list', {
        title: 'Todos',
        todos
    });
});

router.get('/todo_list/create', (req, res) => {
    res.render('todo_create', {
        title: 'Create todo'
    })
});

router.post('/todo_list/create', async (req, res) => {

});

module.exports = router;
